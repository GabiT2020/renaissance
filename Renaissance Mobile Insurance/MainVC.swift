//
//  ViewController.swift
//  Renaissance Mobile Insurance
//
//  Created by Gabriel G on 03/05/2021.
//

import UIKit
import PartnersSDK

class MainVC: UIViewController {
    
    @IBOutlet weak var devLbl: UILabel!{ didSet{
        devLbl.isHidden = !SDKSharedAppData.isDev }}
    @IBOutlet weak var topLbl: UILabel!
    @IBOutlet weak var midLbl: UILabel!
    @IBOutlet weak var bottomLbl: UILabel!
    @IBOutlet weak var topBtn: UIButton!
    @IBOutlet weak var middleBtn: UIButton!
    
    @IBOutlet weak var termsLbl: UITextView!{didSet{
        termsLbl.convert("ren_policy_terms_txt".translateMe,
                         to: ["ren_policy_terms_first_txt".translateMe:"ren_policy_terms_first_url".translateMe,
                              "ren_policy_terms_second_txt".translateMe:"ren_policy_terms_second_url".translateMe])
    }}
    
    var state:PolicyState = .start { didSet{
        if state != .start{
            DispatchQueue.main.async {
                self.performSegue(withIdentifier: "mainTo\(self.state.segueIdentifier)", sender: nil)
            }
        }
    }}
    
    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(refreshPolicyState),
                                               name: UIApplication.willEnterForegroundNotification,
                                               object: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if let statusBar = UIApplication.shared.statusBarUIView {
            statusBar.backgroundColor = UIColor(hex: "#4f287d")
        }
    }
    
    @objc fileprivate func refreshPolicyState() {
        LoadingOverlay.shared.showOverlay(self.view, unBrandedLottie: true)
        
        AppRequest().checkActiveInsurance() { policyName, expireDate, warrantyActive in
            LoadingOverlay.shared.hideOverlayView()
            
            self.state = Utility.getState(policyName != nil, warrantyActive)
            Utility.updatePolicyType(by: policyName ?? "")
        }
    }
    
    @IBAction func getPolicy(_ sender: UIButton) {
        PartnersSDK.analyticsDelegate?.didReport(with: [.event(AEvent.renaissance),
                                                        .category(ACategory.main),
                                                        .action("get_policy")])
        AppRequest().auditLog(event: .getPolicy)
        
        PartnersSDK.showSlides(for: .insurance, needSubmit: true){
            self.refreshPolicyState()
        }
    }
    
    @IBAction func retrievePolicy(_ sender: UIButton) {
        DispatchQueue.main.async {
            let retrieveView = PolicyRetrieveView(delegate: self,
                                                  frame: CGRect(x: 0,
                                                                y: 0,
                                                                width: self.view.bounds.width,
                                                                height: self.view.bounds.height))
            self.view.addSubview(retrieveView)
            retrieveView.popIn()
        }
    }
    
    @IBAction func openMoreInfo(_ sender: UIButton) {
        PartnersSDK.analyticsDelegate?.didReport(with: [.event(AEvent.renaissance),
                                                        .category(ACategory.main),
                                                        .action("more_info")])
        AppRequest().auditLog(event: .moreInfo)
        self.performSegue(withIdentifier: "mainMoreInfo", sender: nil)
    }
}

extension MainVC:PolicyRetrieveViewDelegate{
    func close(retrieveView:PolicyRetrieveView){
        retrieveView.popOut()
        retrieveView.removeFromSuperview()
    }
    
    func presentToast(from retrieveView:PolicyRetrieveView, msg:String){
        self.showToast(message: msg,true)
    }
    
    func verify(mail:String, from retrieveView:PolicyRetrieveView) {
        LoadingOverlay.shared.showOverlay(self.view, unBrandedLottie: true)
        AppRequest().sendValidationMail(to: mail) { sent in
            LoadingOverlay.shared.hideOverlayView()
            if sent{
                retrieveView.firstStage = false
            }else{
                self.showToast(message: "ren_email_validation_error".translateMe,true)
            }
            DispatchQueue.main.async {
                retrieveView.textField.becomeFirstResponder()
            }
        }
    }
    
    func validate(mail:String,with token:String, from retrieveView:PolicyRetrieveView) {
        LoadingOverlay.shared.showOverlay(self.view, unBrandedLottie: true)
        AppRequest().validate(code: token, from: mail) {policyName, expireDate,
                                                        warrantyActive,error in
            LoadingOverlay.shared.hideOverlayView()
            if !error {
                DispatchQueue.main.async {
                    retrieveView.popOut()
                    retrieveView.removeFromSuperview()
                    if policyName != nil{
                        self.state = Utility.getState(true, warrantyActive)
                        Utility.updatePolicyType(by: policyName ?? "")
                    }else{
                        self.showToast(message: "ren_policy_not_valid_error".translateMe,false)
                    }
                }
            }else{
                self.showToast(message: "ren_code_validation_error".translateMe,true)
                DispatchQueue.main.async {
                    retrieveView.textField.becomeFirstResponder()
                }
                
            }
        }
    }
}
