//
//  ViewController.swift
//  Renaissance Mobile Insurance
//
//  Created by Gabriel G on 03/05/2021.
//

import UIKit
import PartnersSDK

class MoreInfoVC: UIViewController {
    
    @IBOutlet weak var descriptionOverlay: UIView!
    @IBOutlet weak var descriptionView: UITextView!
    @IBOutlet weak var descripitionTitle: UILabel!
    @IBOutlet weak var tableView: UITableView!
    
    var list = [MoreInfoElement](){ didSet{
        DispatchQueue.main.async {
            self.tableView.reloadData()
        } }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.delegate = self
        tableView.dataSource = self
        
        LoadingOverlay.shared.showOverlay(self.view, unBrandedLottie: true)
        AppRequest().getMoreInfo { list in
            LoadingOverlay.shared.hideOverlayView()
            if let list = list {
                self.list = list
            }
        }
    }
    
    @IBAction func backed(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func dismissOverlay(_ sender: Any) {
        descriptionOverlay.popOut()
    }
}

extension MoreInfoVC:UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return list.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MoreInfoTableViewCell",
                                                 for: indexPath)
        if let cell = cell as? MoreInfoTableViewCell {
            cell.cellTitle.text = list[indexPath.row].question
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let answer = list[indexPath.row].answer,
           let stringData = answer.data(using: .unicode,
                                         allowLossyConversion: true){
            let attributedString = try? NSAttributedString(
                data: stringData,
                options:[.documentType:NSAttributedString.DocumentType.html],
                documentAttributes: nil)
            descriptionView.attributedText = attributedString
            descripitionTitle.text = list[indexPath.row].question
            descriptionOverlay.popIn()
        }
    }
}


class MoreInfoTableViewCell:UITableViewCell{
    
    @IBOutlet weak var cellTitle: UILabel!
    
}
