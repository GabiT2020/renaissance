//
//  Utility.swift
//  Renaissance Mobile Insurance
//
//  Created by Gabriel G on 03/05/2021.
//

import UIKit
import PartnersSDK

class Utility{
    
    static var insuranceType:InsuranceType = .extanded
    
    static func getState(_ isInsured:Bool, _ isActive:Bool)->PolicyState{
        if !isInsured{
            return PolicyState.start
        }else if !isActive{
            return PolicyState.progress
        }else{
            return PolicyState.done
        }
    }
    
    static func updatePolicyType(by name:String){
        let locale = Locale(identifier: "ru")
        Utility.insuranceType = name.lowercased(with: locale).contains("экран") ? .screen:.extanded
    }
    
    static var appShareURL: String {
        return "https://renas.page.link/qbvQ"
    }
    
    static func setReminder(days value:Int) {
        if let date = Calendar.current.date(byAdding: .day,
                                            value: value,
                                            to: Date()) {
            let content = UNMutableNotificationContent()
            content.title = "ren_notification_title".translateMe
            content.body = "ren_notification_msg".translateMe
            content.subtitle = "ren_notification_subtitle".translateMe
            content.sound = UNNotificationSound.default
            let triggerDate = Calendar.current.dateComponents([.year, .month, .day,
                                                               .hour, .minute, .second], from: date)
            let trigger = UNCalendarNotificationTrigger(dateMatching: triggerDate, repeats: false)
            let request = UNNotificationRequest(identifier: UUID().uuidString,
                                                content: content,
                                                trigger: trigger)
            UNUserNotificationCenter.current().add(request)
        }
    }
}

enum PolicyState{
    case start,progress,done
    
    var segueIdentifier:String{
        switch self {
        case .start: return "Start"
        case .progress: return "Progress"
        case .done: return "Done"
        }
    }
}

enum InsuranceType{
    case screen,extanded
    
    var name:String{
        return self == .screen ? "screen":"extanded"
    }
}
