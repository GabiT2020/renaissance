//
//  ViewController.swift
//  Renaissance Mobile Insurance
//
//  Created by Gabriel G on 03/05/2021.
//

import UIKit
import PartnersSDK

class SerialVC: UIViewController {
    
    @IBOutlet weak var topLbl: UILabel!
    @IBOutlet weak var serialTf: UITextField!
    @IBOutlet weak var pastBtn: UIButton!
    
    var pasteState:Bool = true
    var serialNumber = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        serialTf.addDoneCancelToolbar()
        topLbl.attributedText = "ren_serial_top_txt".translateAndBoldMe
    }
    
    @IBAction func serialInputMade(_ sender: UIButton){
        if !serialNumber.isEmpty{
            LoadingOverlay.shared.showOverlay(self.view, unBrandedLottie: true)
            AppRequest().setSerial(number: self.serialNumber) { updated in
                if updated{
                    PartnersSDK.submitData(to: .warranty) {
                        AppRequest().createCertificate {
                            DispatchQueue.main.async {
                                LoadingOverlay.shared.hideOverlayView()
                                self.performSegue(withIdentifier: "serialToCertificated", sender: nil)
                            }
                        }
                    }
                }else{
                    LoadingOverlay.shared.hideOverlayView()
                    self.showToast(message: "ren_serial_update_error".translateMe)
                }
            }
        }else{
            self.showToast(message: "ren_serial_update_error".translateMe)
        }
    }
    
    @IBAction func pasteValue(_ sender: UIButton){
        DispatchQueue.main.async {
            if !self.pasteState{
                self.serialTf.text = ""
                self.pastBtn.setTitle("ren_paste_serial_btn".translateMe, for: .normal)
                self.serialNumber = ""
                self.pasteState = true
            }else if let string = UIPasteboard.general.string{
                self.serialTf.text = string
                self.serialNumber = string
                self.pastBtn.setTitle("ren_clear_serial_btn".translateMe, for: .normal)
                self.pasteState = false
            }
        }
    }
}

class SerialInstructionsVC: UIViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
    }
}

