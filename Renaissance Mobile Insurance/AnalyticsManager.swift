//
//  AnalyticsManager.swift
//  TestM
//
//  Created by TestM IOS on 05/03/2018.
//  Copyright © 2018 Veesto. All rights reserved.
//

import Foundation
import FirebaseAnalytics
import PartnersSDK

class AnalyticsManager: NSObject {

    enum Trackers {
        case google([AnalyticsComponents])
    }

    class func track(to providers: Trackers...) {
        if !SDKSharedAppData.isDev {
            for provider in providers {
                switch provider {
                case .google(let components):
                    FireBaseManager.track(with: components)
                }
            }
        }
    }
}

class FireBaseManager: AnalyticsManager {

    class func track(with components: [AnalyticsComponents]) {
        var eventName: String = "Undefined"
        var trackParams = [String: Any]()

        for component in components {
            switch component {
            case .action(let action):
               trackParams["action"] = action
            case .category(let category):
               trackParams["category"] = category
            case .label(let label):
               trackParams["label"] = label
            case .event(let event):
                eventName = event
            }
        }
        Analytics.logEvent(eventName, parameters: trackParams)
    }
}
