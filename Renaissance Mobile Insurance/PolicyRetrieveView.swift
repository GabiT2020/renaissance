//
//  TouchAccuracyVC.swift
//  micCheck
//
//  Created by TestM IOS on 31/07/2019.
//  Copyright © 2019 TestM. All rights reserved.
//

import UIKit
import PartnersSDK

protocol PolicyRetrieveViewDelegate:AnyObject {
    func close(retrieveView:PolicyRetrieveView)
    func presentToast(from retrieveView:PolicyRetrieveView, msg:String)
    func verify(mail:String, from retrieveView:PolicyRetrieveView)
    func validate(mail:String,with token:String, from retrieveView:PolicyRetrieveView)
}

class PolicyRetrieveView: UIView {
    
    @IBOutlet var titleLbl: UILabel!
    @IBOutlet var subTitleLbl: UILabel!
    @IBOutlet var textField: UITextField!{didSet{
        textField.addDoneCancelToolbar(onDone: (target: self,
                                                action: #selector(doneButtonTapped)),
                                       onCancel: (target: self,
                                                  action: #selector(cancelButtonTapped)))
    }}
    @IBOutlet var cancelBtn: UIButton!
    @IBOutlet var sendBtn: UIButton!
    
    weak var delegate:PolicyRetrieveViewDelegate?
   
    var mailAddress = ""
    let nibName: String = "PolicyRetrieveView"
    var view: UIView!
    var firstStage = true{
        didSet{
            DispatchQueue.main.async {
                self.titleLbl.text = (self.firstStage ? "ren_retrieve_policy_alert_title":"ren_validate_policy_mail_title").translateMe
                self.subTitleLbl.text = (self.firstStage ? "ren_retrieve_policy_alert_msg":"ren_validate_policy_mail_msg").translateMe
                self.textField.placeholder = (self.firstStage ? "ren_retrieve_policy_enter_mail":"ren_validate_policy_enter_code").translateMe
                self.cancelBtn.setTitle((self.firstStage ? "ren_retrieve_policy_cancel":"ren_validate_policy_cancel").translateMe, for: .normal)
                self.sendBtn.setTitle((self.firstStage ? "ren_retrieve_policy_ok":"ren_validate_policy_ok").translateMe, for: .normal)
                self.textField.keyboardType = self.firstStage ? .emailAddress:.numberPad
            }
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        initialize()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initialize()
    }
    
    func initialize() {
        let nib = UINib(nibName: nibName, bundle: .main)
        view = nib.instantiate(withOwner: self, options: nil)[0] as? UIView
        view.frame = bounds
        view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        addSubview(view)
    }
    
    init(delegate: PolicyRetrieveViewDelegate, frame: CGRect) {
        super.init(frame: frame)
        initialize()
        self.delegate = delegate
        textField.becomeFirstResponder()
    }
    
    @objc func doneButtonTapped() {
        self.resignFirstResponder()
        sendClicked(sendBtn)
    }
    @objc func cancelButtonTapped() {
        self.resignFirstResponder()
        cancelClicked(cancelBtn)
    }
    
    @IBAction func sendClicked(_ sender: UIButton) {
        textField.resignFirstResponder()
        if firstStage{
            if let address = textField.text,
               address.isValidEmail{
                self.mailAddress = address
                self.textField.text = ""
                delegate?.verify(mail:address, from:self)
            }else{
                self.delegate?.presentToast(from: self,
                                            msg: "ren_retrieve_policy_email_error".translateMe)
                self.textField.text = ""
                self.textField.becomeFirstResponder()
            }
        }else if let token = textField.text,
                 !token.isEmpty{
            delegate?.validate(mail: mailAddress, with: token,from: self)
        }
    }
    
    @IBAction func cancelClicked(_ sender: UIButton) {
        delegate?.close(retrieveView: self)
    }
}
