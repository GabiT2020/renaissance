//
//  ViewController.swift
//  Renaissance Mobile Insurance
//
//  Created by Gabriel G on 03/05/2021.
//

import UIKit
import PartnersSDK

class SignedPolicyVC: UIViewController {
    @IBOutlet weak var devLbl: UILabel!{ didSet{
        devLbl.isHidden = !SDKSharedAppData.isDev }}
    
    @IBOutlet weak var topLbl: UILabel!
    @IBOutlet weak var midLbl: UILabel!
    @IBOutlet weak var bottomLbl: UILabel!
    
    @IBOutlet weak var topBtn: UIButton!
    @IBOutlet weak var middleBtn: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        Utility.setReminder(days:3)
        Utility.setReminder(days:10)
        Utility.setReminder(days:20)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if let statusBar = UIApplication.shared.statusBarUIView {
            statusBar.backgroundColor = UIColor(hex: "#4f287d")
        }
    }
    
    fileprivate func presentTests() {
        if Utility.insuranceType == .screen{
            PartnersSDK.startCustom(list: [(.brokenScreen,true)],
                                    under: .insurance) {
                PartnersSDK.showSummary {
                    self.showCertUpdateDialog()
                }
            }
        }else{
            PartnersSDK.startRun(for: .insurance, [.imei]) {
                PartnersSDK.showSummary {
                    self.showCertUpdateDialog()
                }
            }
        }
    }
    
    @IBAction func startTests(_ sender: UIButton) {
        PartnersSDK.analyticsDelegate?.didReport(with: [.event(AEvent.renaissance),
                                                        .category(ACategory.main),
                                                        .action("get_warranty")])
        AppRequest().auditLog(event: .getWarranty)
        
        let alert = UIAlertController(title: "ren_pretest_\(Utility.insuranceType.name)_alert_title".translateMe,
                                      message: "ren_pretest_\(Utility.insuranceType.name)_alert_msg".translateMe,
                                      preferredStyle: .alert)
        let action = UIAlertAction(title: "ren_pretest_\(Utility.insuranceType.name)_alert_yes".translateMe,
                                   style: .default) { _ in
            self.presentTests()
        }
        let cancel = UIAlertAction(title: "ren_pretest_\(Utility.insuranceType.name)_alert_no".translateMe,
                                   style: .cancel,
                                   handler: nil)
        DispatchQueue.main.async {
            alert.addAction(action)
            alert.addAction(cancel)
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    @IBAction func openMoreInfo(_ sender: UIButton) {
        PartnersSDK.analyticsDelegate?.didReport(with: [.event(AEvent.renaissance),
                                                        .category(ACategory.main),
                                                        .action("more_info")])
        AppRequest().auditLog(event: .moreInfo)
        self.performSegue(withIdentifier: "signedMoreInfo", sender: nil)
    }
    
    @IBAction func sendPolicyToClientMail(_ sender: UIButton) {
        PartnersSDK.analyticsDelegate?.didReport(with: [.event(AEvent.renaissance),
                                                        .category(ACategory.main),
                                                        .action("download_policy")])
        AppRequest().auditLog(event: .downloadPolicy)
        AppRequest().sendPolicyToClientMail(){
            self.showToast(message: "check_mail_toast".translateMe)
        }
    }
    
    func showCertUpdateDialog(){
        
        var alertTitle = ""
        var alertMsg = ""
        var alertOKAction = {}
        var alertOKLbl = ""
        var alertCancelLbl = ""
        var alertCancelAction = {}
        
        let remmainingTestsRate = SDKSharedAppData.currentReport.remmainingTestsRate
        if remmainingTestsRate == 0{
            self.performSegue(withIdentifier: "signedToSerialSegue", sender: nil)
            return
        }else if remmainingTestsRate == 100{
            alertTitle = "ren_0_percentage_test_title".translateMe
            alertMsg = "ren_0_percentage_test_message".translateMe
            alertOKLbl = "ren_0_percentage_test_confirm".translateMe
            alertCancelLbl = "ren_0_percentage_test_cancel".translateMe
            alertOKAction = {self.presentTests()}
        }else{
            alertTitle = "ren_found_damage_test_title".translateMe
            alertMsg = "ren_found_damage_test_message".translateMe
            alertOKLbl = "ren_found_damage_test_confirm".translateMe
            alertCancelLbl = "ren_found_damage_test_cancel".translateMe
            alertOKAction = {self.performSegue(withIdentifier: "signedToSerialSegue", sender: nil)}
            alertCancelAction = {self.presentTests()}
        }
        
        let alert = UIAlertController(title: alertTitle,
                                      message: alertMsg,
                                      preferredStyle: .alert)
        let yes = UIAlertAction(title: alertOKLbl,
                                style: .default) { _ in
            alertOKAction()
        }
        let no = UIAlertAction(title: alertCancelLbl,
                               style: .cancel){_ in
            alertCancelAction()
        }
        alert.addAction(yes)
        alert.addAction(no)
        self.present(alert, animated: true, completion: nil)
    }
}

