//
//  ViewController.swift
//  Renaissance Mobile Insurance
//
//  Created by Gabriel G on 03/05/2021.
//

import UIKit
import PartnersSDK


class CertificateDoneVC: UIViewController {
    
    @IBOutlet weak var devLbl: UILabel!{ didSet{
        devLbl.isHidden = !SDKSharedAppData.isDev }}
    
    @IBOutlet weak var topLbl: UILabel!
    @IBOutlet weak var midLbl: UILabel!
    @IBOutlet weak var bottomLbl: UILabel!
    
    @IBOutlet weak var middleBtn: UIButton!
    
    @IBOutlet weak var shareView: UIStackView!
    @IBOutlet weak var shareIcon: UIImageView!{didSet{
        shareIcon.image = UIImage.bundledImage(named: "shareIcon")
    }}
    @IBOutlet weak var shareAppBtn: UIButton!
    @IBOutlet weak var getCertificateRecord: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if let statusBar = UIApplication.shared.statusBarUIView {
            statusBar.backgroundColor = UIColor(hex: "#4f287d")
        }
    }
    
    @IBAction func openMoreInfo(_ sender: UIButton) {
        PartnersSDK.analyticsDelegate?.didReport(with: [.event(AEvent.renaissance),
                                                        .category(ACategory.main),
                                                        .action("more_info")])
        AppRequest().auditLog(event: .moreInfo)
        self.performSegue(withIdentifier: "doneMoreInfo", sender: nil)
    }
    
    @IBAction func sendPolicyToClientMail(_ sender: UIButton) {
        PartnersSDK.analyticsDelegate?.didReport(with: [.event(AEvent.renaissance),
                                                        .category(ACategory.main),
                                                        .action("download_policy")])
        AppRequest().auditLog(event: .downloadPolicy)
        AppRequest().sendPolicyToClientMail(){
            self.showToast(message: "check_mail_toast".translateMe)
        }
    }
    
    @IBAction func getCertificate(_ sender: UIButton) {
        PartnersSDK.analyticsDelegate?.didReport(with: [.event(AEvent.renaissance),
                                                        .category(ACategory.main),
                                                        .action("download_warranty")])
        AppRequest().auditLog(event: .downloadWarranty)
        AppRequest().sendCertificate {
            self.showToast(message: "check_mail_toast".translateMe)
        }
    }
    
    @IBAction func shareApp(_ sender: UIButton) {
        PartnersSDK.analyticsDelegate?.didReport(with: [.event(AEvent.renaissance),
                                                        .category(ACategory.main),
                                                        .action("share_app")])
        AppRequest().auditLog(event: .shareApp)
        DispatchQueue.main.async {
            let textToShare = "ren_app_share_msg".translateMe
            let urlToShare = Utility.appShareURL
            let objectsToShare = [textToShare, urlToShare] as [Any]
            let shareVC = UIActivityViewController(activityItems: objectsToShare,
                                                   applicationActivities: nil)
            DispatchQueue.main.async {
                if UIDevice.current.userInterfaceIdiom == .pad{
                    shareVC.popoverPresentationController?.sourceView = self.view
                    shareVC.popoverPresentationController?.sourceRect = CGRect(x: self.view.bounds.minX,
                                                                               y: self.view.bounds.midY+100,
                                                                               width: 0,
                                                                               height: 0)
                }
                self.present(shareVC, animated: true, completion: nil)
            }
        }
    }
    
}
