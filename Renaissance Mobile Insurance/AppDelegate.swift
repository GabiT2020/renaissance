//
//  AppDelegate.swift
//  Renaissance Mobile Insurance
//
//  Created by Gabriel G on 03/05/2021.
//

import UIKit
import PartnersSDK
import Firebase

@main
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication,
                     didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        PartnersSDK.analyticsDelegate = self
        FirebaseApp.configure()
        
        didReport(with: [.event(AEvent.renaissance),
                         .category(ACategory.session),
                         .action(AAction.open)])
        AppRequest().auditLog(event: .appStart)
        SDKSharedAppData.logsPending.forEach {
            AppRequest().resendAudit($0 )
        }
        
        UNUserNotificationCenter.current().removeAllPendingNotificationRequests()
        UNUserNotificationCenter.current().requestAuthorization(options: [.badge, .alert, .sound]) { (_, _) in }
        application.registerForRemoteNotifications()
        
        return true
    }

    func application(_ app: UIApplication, open url: URL,
                     options: [UIApplication.OpenURLOptionsKey: Any] = [:]) -> Bool {
        return application(app, open: url,
                           sourceApplication: options[UIApplication.OpenURLOptionsKey.sourceApplication] as? String,
                           annotation: "")
    }

    func application(_ application: UIApplication, open url: URL,
                     sourceApplication: String?, annotation: Any) -> Bool {
        if let dynamicLink = DynamicLinks.dynamicLinks().dynamicLink(fromCustomSchemeURL: url),
                 let deepLinkURL = dynamicLink.url {
            handle(deepLinkURL)
            return true
        }
        return false
    }

    func application(_ application: UIApplication, continue userActivity: NSUserActivity,
                     restorationHandler: @escaping ([UIUserActivityRestoring]?) -> Void) -> Bool {
        if let url = userActivity.webpageURL {
            let handled = DynamicLinks.dynamicLinks().handleUniversalLink(url) { (dynamiclink, _) in
                if let dynamicLink = dynamiclink, let deepLinkURL = dynamicLink.url {
                    self.handle(deepLinkURL)
                }
            }
            return handled
        }
        return false
    }

    func handle(_ url: URL) {
        if let queryItems = URLComponents(string: url.absoluteString)?.queryItems {
            DeepLinkManager.setDeepLink(from: queryItems)
        }
    }
}

// MARK: - SDK Analytics delegate -
extension AppDelegate: AnalyticsManagerDelegate {
    func didReport(with components: [AnalyticsComponents]) {
        AnalyticsManager.track(to: .google(components))
    }
}
