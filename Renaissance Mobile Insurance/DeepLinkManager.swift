//
//  DeepLinkManager.swift
//  TestM
//
//  Created by TestM IOS on 02/04/2018.
//  Copyright © 2018 Veesto. All rights reserved.
//

import Foundation
import PartnersSDK

public class DeepLink: NSObject {

    public var flowType: DeepLinkType?

    public init(_ flow: DeepLinkType?,
                _ code: String? = "",
                _ agentId: String? = "") {
        super.init()
        self.flowType = flow
  
        SDKSharedAppData.labCode = code ?? ""
        SDKSharedAppData.saveLabCode()
        SDKSharedAppData.agentId = agentId ?? ""
    }
}

public class DeepLinkManager: NSObject {

    public static var DeepLinkInQueue: DeepLink? { didSet {
        if DeepLinkInQueue != nil {
            NotificationCenter.default.post(name: .deepLinkRecieved, object: nil)
        } }
    }

    public class func setDeepLink(from userInfo: [AnyHashable: Any]) {
        UserDefaults.standard.deeplinkQuery = userInfo.asURLRequestParams
        let flowType = DeepLinkType(rawValue: userInfo[DeepLinkKeys.flowType] as? String ?? "")
        let agentId = userInfo[DeepLinkKeys.agentId] as? String
        let code = userInfo[DeepLinkKeys.storeCode] as? String

        DeepLinkManager.DeepLinkInQueue = DeepLink(flowType,
                                                   code,
                                                   agentId)
    }

    public class func setDeepLink(from queryItems: [URLQueryItem]) {
        var queryParams = [String:Any]()
        queryItems.forEach { (item) in
            queryParams[item.name] = item.value
        }
        UserDefaults.standard.deeplinkQuery = queryParams.asURLRequestParams
        
        let flowType = DeepLinkType(rawValue: queryItems.first(where: {$0.name == DeepLinkKeys.flowType.rawValue})?.value ?? "")
        let agentId = queryItems.first(where: {$0.name == DeepLinkKeys.agentId.rawValue})?.value
        let code = queryItems.first(where: {$0.name == DeepLinkKeys.storeCode.rawValue})?.value
      
        DeepLinkManager.DeepLinkInQueue = DeepLink(flowType,
                                                   code,
                                                   agentId)
    }
}

enum DeepLinkKeys: String {
    case flowType = "flow_type"
    case storeCode = "store_code"
    case agentId = "agent_id"
}

public enum DeepLinkType: String {
    case startInsurance = "insurance"
}

extension NSNotification.Name {
    static let deepLinkRecieved = NSNotification.Name("deepLinkRecieved")
    static let insuranceStateChanged = NSNotification.Name("stateChanged")
}
