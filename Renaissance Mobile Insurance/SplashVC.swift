//
//  ViewController.swift
//  Renaissance Mobile Insurance
//
//  Created by Gabriel G on 03/05/2021.
//

import UIKit
import PartnersSDK

class SplashVC: UIViewController {
    
    var policyName:String?
    var expireDate:String?
    var warrantyActive = false
    
    override func viewDidLoad() {
        super.viewDidLoad()

        PartnersSDK.connectToServer(with: "25104") { success, error in
            if success, error == nil{
                AppRequest().checkActiveInsurance { policyName, expireDate, warrantyActive in

                    self.policyName = policyName
                    self.expireDate = expireDate
                    self.warrantyActive = warrantyActive

                   let state = Utility.getState(self.policyName != nil, warrantyActive)
                   Utility.updatePolicyType(by: policyName ?? "")

                    DispatchQueue.main.async {
                        self.performSegue(withIdentifier: "splashTo\(state.segueIdentifier)",
                                          sender: nil)
                    }
                }
            }
        }
    }
}

